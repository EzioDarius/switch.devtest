﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Customer
    {
        public string Name { get; private set; }
        private List<BaseAccount> accounts { get; set; }
        public Customer(string name)
        {
            Name = name;
            accounts = new List<BaseAccount>();
        }
        public Customer OpenAccount(BaseAccount account)
        {
            accounts.Add(account);
            return this;
        }
        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }
        public decimal TotalInterestEarned()
        {
            decimal total = 0;
            foreach (var a in accounts)
                total += a.InterestAccrued();
            return total;
        }

        public string GetStatement()
        {
            StringBuilder statement = new StringBuilder();
            statement.AppendLine($"Statement for {Name}").AppendLine();
            decimal total = 0;
            foreach (var a in accounts)
            {
                statement.AppendLine(a.GetStatement());
                total += a.GetBalanceWithInterests();
            }
            statement.Append($"Total In All Accounts {ToDollars(total)}");
            return statement.ToString();
        }

        private String ToDollars(decimal d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }
    }
}
