﻿using SharpBank.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class MaxiSavingsAccount :BaseAccount
    {
        private decimal NoWithdrawsRate { get; set; }
        private decimal MoneyWithdrawnLast10DaysRate { get; set; }
        public MaxiSavingsAccount() : this(new Dictionary<DateTime, DayTransactionsHeader>())
        {
        }
        public MaxiSavingsAccount(Dictionary<DateTime, DayTransactionsHeader> transactionHeaders) : base(transactionHeaders)
        {
            PrettyName = Resources.MaxiSavingsAccount_PrettyName;
            NoWithdrawsRate = 5m / 365 / 100;
            MoneyWithdrawnLast10DaysRate = 0.1m / 365 / 100;
        }
        public override decimal InterestAccrued()
        {
            decimal accuredInterest = 0;
            decimal priorDayInterest = 0;
            decimal endingBalance = 0;
            int NoWithdrawsCounter = 10;
            if (TransactionHeaders.Count() > 0)
            {
                var dateFrom = TransactionHeaders.First().Key;
                for (var currentDate = dateFrom; currentDate <= DateTime.Today; currentDate = currentDate.AddDays(1))
                {
                    var currentDayHaveWithdraw = false;
                    endingBalance += priorDayInterest;
                    
                    if (TransactionHeaders.TryGetValue(currentDate, out DayTransactionsHeader result))
                    {
                        endingBalance += result.AmountSum;
                        currentDayHaveWithdraw = result.HasWithdraw;
                    }
                    NoWithdrawsCounter = currentDayHaveWithdraw ? 0 : NoWithdrawsCounter + 1;
                    priorDayInterest = endingBalance * (NoWithdrawsCounter <= 10 ? MoneyWithdrawnLast10DaysRate : NoWithdrawsRate);
                    accuredInterest += priorDayInterest;

                }
            }
            return accuredInterest;
        }
    }
}
