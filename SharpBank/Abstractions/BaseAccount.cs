using System;
using System.Collections.Generic;
using System.Text;
using SharpBank.Properties;

namespace SharpBank
{
    public abstract class BaseAccount
    {
        public string PrettyName { get; protected set; }
        private decimal currentBalance { get;  set; }
        public Dictionary<DateTime, DayTransactionsHeader> TransactionHeaders { get; protected set; }
        public BaseAccount(Dictionary<DateTime, DayTransactionsHeader> transactionHeaders)
        {
            TransactionHeaders = transactionHeaders != null ? transactionHeaders: new Dictionary<DateTime, DayTransactionsHeader>();
        }
        public abstract decimal InterestAccrued();
        public string GetStatement()
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine(PrettyName);
            foreach (var amount in GetTransactionList())
            {
                result.AppendLine($"  {(amount < 0 ? "withdrawal" : "deposit")} {ToDollars(amount)}");
            }
            var interestsGenerated = InterestAccrued();
            result.AppendLine($"Interests {ToDollars(interestsGenerated)}");
            result.AppendLine($"Total {ToDollars(currentBalance + interestsGenerated)}");
            return result.ToString();
        }
        public List<decimal> GetTransactionList()
        {
            List<decimal> resultList = new List<decimal>();
            foreach (var item in TransactionHeaders)
            {
                resultList.AddRange(item.Value.Movements);
            }
            return resultList;
        }
        public BaseAccount Deposit(decimal amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException(Resources.AccountErrorMessageInvalidAmount);
            }
            if (!TransactionHeaders.ContainsKey(DateTime.Today))
            {
                TransactionHeaders.Add(DateTime.Today, new DayTransactionsHeader());
            }
            currentBalance += amount;
            TransactionHeaders[DateTime.Today].Add(amount);
            return this;
        }
        public decimal GetBalanceWithInterests() {
            return currentBalance + InterestAccrued();
        }
        public BaseAccount Withdraw(decimal amount)
        {
            var actualBalance = GetBalanceWithInterests();
            if (amount > actualBalance)
            {
                throw new ArgumentException($"Amount {ToDollars(amount)} cannot be withdrawn , available balance is {ToDollars(actualBalance) }.");
            }
            if (amount <= 0)
            {
                throw new ArgumentException(Resources.AccountErrorMessageInvalidAmount);
            }
            if (!TransactionHeaders.ContainsKey(DateTime.Today))
            {
                TransactionHeaders.Add(DateTime.Today, new DayTransactionsHeader());
            }
            currentBalance -= amount;
            TransactionHeaders[DateTime.Today].Add(-amount);
            return this;
        }
        public void TransferMoneyTo(BaseAccount target, decimal amount)
        {
            try
            {
                Withdraw(amount);
                target.Deposit(amount);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
        }
        private String ToDollars(decimal d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }
       


    }

}