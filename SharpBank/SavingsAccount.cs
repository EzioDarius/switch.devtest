﻿using SharpBank.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class SavingsAccount:BaseAccount
    {
        private decimal Rate0;
        private decimal Rate1000;
        public SavingsAccount() : this(new Dictionary<DateTime, DayTransactionsHeader>())
        {
        }
        public SavingsAccount(Dictionary<DateTime, DayTransactionsHeader> transactionHeaders) : base(transactionHeaders)
        {
            PrettyName = Resources.SavingsAccount_PrettyName;
            Rate0 = (0.1m / 365) / 100;
            Rate1000 = (0.2m / 365) / 100;
        }
        public override decimal InterestAccrued()
        {
            decimal accuredInterest = 0;
            decimal priorDayInterest = 0;
            decimal endingBalance = 0;
            if (TransactionHeaders.Count() > 0)
            {
                var dateFrom = TransactionHeaders.First().Key;
                for (var currentDate = dateFrom; currentDate <= DateTime.Today; currentDate = currentDate.AddDays(1))
                {
                    endingBalance += priorDayInterest;
                    if (TransactionHeaders.TryGetValue(currentDate, out DayTransactionsHeader result))
                    {
                        endingBalance += result.AmountSum;
                    }
                    priorDayInterest = ApplyRate(endingBalance); 
                    accuredInterest += priorDayInterest;
                }
            }
            return accuredInterest;         
        }
        private decimal ApplyRate(decimal amount)
        {
            if (amount <= 1000)
                return amount * Rate0;
            else
                return 1000 * Rate0 + ((amount - 1000) * Rate1000);
        }
    }
}
