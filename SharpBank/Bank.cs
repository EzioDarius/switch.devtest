﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Bank
    {
        public List<Customer> customers { get; protected set ; }
        public Bank()
        {
            customers = new List<Customer>();
        }
        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
        }
        public string CustomerSummary()
        {
            StringBuilder summary = new StringBuilder();
            summary.AppendLine("Customer Summary");
            foreach (Customer c in customers)
            {
                summary.AppendLine(" - " + c.Name + " (" + Pluralize(c.GetNumberOfAccounts(), "account") + ")");
            }
            return summary.ToString();
        }
        private string Pluralize(int number, string word)
        {
            return number + " " + (number == 1 ? word : word + "s");
        }
        public decimal TotalInterestPaid()
        {
            decimal total = 0;
            foreach (Customer c in customers)
                total += c.TotalInterestEarned();
            return total;
        }
    }
}
