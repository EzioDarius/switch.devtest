﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class DayTransactionsHeader
    {
        public List<decimal> Movements { get; private set; }
        public decimal AmountSum { get; private set; }
        public bool HasWithdraw { get; private set; }
        public DayTransactionsHeader()
        {
            Movements = new List<decimal>();
        }
        public void Add(decimal amount)
        {
            AmountSum += amount;
            HasWithdraw = !HasWithdraw && amount < 0;
            Movements.Add(amount);
        }
    }
}
