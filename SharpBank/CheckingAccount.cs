﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpBank.Properties;

namespace SharpBank
{
    public class CheckingAccount : BaseAccount
    {
        private decimal dailyInterestRate { get; set; }
        public CheckingAccount():this(new Dictionary<DateTime, DayTransactionsHeader>())
        {
        }
        public CheckingAccount(Dictionary<DateTime, DayTransactionsHeader> transactionHeaders):base(transactionHeaders)
        {
            PrettyName = Resources.CheckingAccount_PrettyName;
            dailyInterestRate = (0.1m / 365) / 100;
        }
        public override decimal InterestAccrued()
        {
            decimal accuredInterest = 0;
            decimal priorDayInterest = 0;
            decimal endingBalance = 0;
            if (TransactionHeaders.Count() > 0)
            {
                var dateFrom = TransactionHeaders.First().Key;

                for (var currentDate = dateFrom; currentDate <= DateTime.Today; currentDate = currentDate.AddDays(1))
                {
                    endingBalance += priorDayInterest;
                    if (TransactionHeaders.TryGetValue(currentDate, out DayTransactionsHeader result))
                    {
                        endingBalance += result.AmountSum;
                    }
                    priorDayInterest = endingBalance * dailyInterestRate;
                    accuredInterest += priorDayInterest;
                }
            }
            return accuredInterest;
        }
    }
}
