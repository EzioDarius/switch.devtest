﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest: TestBase
    {
        private Bank bank { get; set; }
        private decimal DECIMAL_DELTA => 1e-15m;
        [SetUp]
        public void Init()
        {
            bank = new Bank();
        }

        [Test]
        public void AddCustomerTest()
        {
            bank.AddCustomer(new Customer("David"));
            Assert.AreEqual("David", bank.customers.First().Name);
        }


        [Test]
        public void CustomerSummaryTest()
        {
            bank.AddCustomer(new Customer("John").OpenAccount(new CheckingAccount()));
            Assert.AreEqual("Customer Summary\r\n - John (1 account)\r\n", bank.CustomerSummary());
        }


        [Test]
        public void EmptyBankSummaryTest()
        {
            Assert.AreEqual("Customer Summary\r\n", bank.CustomerSummary());
        }

        [Test]
        public void EmptyBankInterestTest()
        {
            Assert.AreEqual(0, bank.TotalInterestPaid());
        }
        [Test]
        public void CheckingAccountInterestTest()
        {
            bank.AddCustomer(new Customer("Bill").OpenAccount(new CheckingAccount().Deposit(100)));
            AssertDiff(0.00027397260273972602739726m, bank.TotalInterestPaid());
        }

        [Test]
        public void SavingsAccountInterestTest()
        {
            bank.AddCustomer(new Customer("Bill").OpenAccount(new SavingsAccount().Deposit(1500)));
            AssertDiff(0.0054794520547945205479452m, bank.TotalInterestPaid());
        }

        [Test]
        public void MaxiSavingsAccountInterestTest()
        {
            bank.AddCustomer(new Customer("Bill").OpenAccount(new MaxiSavingsAccount().Deposit(3000)));
            AssertDiff(0.4109589041095890410958903m, bank.TotalInterestPaid());
        }
        [TearDown]
        public void TearDown()
        {
            bank = null;
        }
    }
}
