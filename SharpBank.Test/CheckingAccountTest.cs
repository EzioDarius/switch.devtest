﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CheckingAccountTest: TestBase
    {
        private CheckingAccount checkingAccount { get; set; }


        [SetUp]
        public void Init() {
            checkingAccount = new CheckingAccount();

        }
        [Test]
        public void DepositTest()
        {
            checkingAccount.Deposit(20);
            Assert.AreEqual(20, checkingAccount.GetTransactionList().FirstOrDefault());
        }

        [Test]
        public void WithdrawTest()
        {
            checkingAccount.Deposit(20);
            checkingAccount.Withdraw(10);
            Assert.AreEqual(-10, checkingAccount.GetTransactionList().FirstOrDefault(x=> x < 0));
        }

        [Test]
        public void InterestEarnedTest()
        {
            checkingAccount.Deposit(200);
            AssertDiff(0.00054794520547945205479452m, checkingAccount.InterestAccrued());
        }

        [Test]
        public void InterestEarnedEmptyTest()
        {
            Assert.AreEqual(0, checkingAccount.InterestAccrued());
        }

        [Test]
        public void ErrorIfAmountGreaterThanBalance()
        {
            checkingAccount.Deposit(1500);
            Assert.Throws(Is.TypeOf<ArgumentException>().And.Message.EqualTo("Amount $2,000.00 cannot be withdrawn , available balance is $1,500.00."), () => checkingAccount.Withdraw(2000));
        }
        [Test]
        public void ErrorIfAmountNegativeDeposit()
        {
            
            Assert.Throws(Is.TypeOf<ArgumentException>().And.Message.EqualTo("amount must be greater than zero"), () => checkingAccount.Deposit(-1500));
        }

        [Test]
        public void ErrorIfAmountNegativeWithdraw()
        {
           
            Assert.Throws(Is.TypeOf<ArgumentException>().And.Message.EqualTo("amount must be greater than zero"), () => checkingAccount.Withdraw(-1500));
        }

        [Test]
        public void TransferBetweenAccounts()
        {
            var savingsAccount = new SavingsAccount();
            checkingAccount.Deposit(1500);
            checkingAccount.TransferMoneyTo(savingsAccount, 1400);
            AssertDiff(100.00027397260273972602739726m, checkingAccount.GetBalanceWithInterests());
        }

        [Test]
        public void InterestAccruedInDaysTest()
        {
            var transactionList = new Dictionary<DateTime, DayTransactionsHeader>();
            var transactionDate = DateTime.Today.AddDays(-10);
            var header = new DayTransactionsHeader();
            header.Add(100);
            header.Add(200);
            transactionList.Add(transactionDate, header);
            transactionDate = DateTime.Today.AddDays(-5);
            header = new DayTransactionsHeader();
            header.Add(400);
            transactionList.Add(transactionDate, header);
            var checkingAccountWithTransactions = new CheckingAccount(transactionList);

            AssertDiff(0.0156166072445677240361689175m, checkingAccountWithTransactions.InterestAccrued());
        }

        [TearDown]
        public void TearDown()
        {
            checkingAccount = null;
        }
    }
}
