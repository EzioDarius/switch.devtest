﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class SavingsAccountTest : TestBase
    {
        [Test]
        public void InterestAccruedInDaysTest()
        {
            var transactionList = new Dictionary<DateTime, DayTransactionsHeader>();
            var transactionDate = DateTime.Today.AddDays(-11);
            var header = new DayTransactionsHeader();
            header.Add(500);
            transactionList.Add(transactionDate, header);
            transactionDate = DateTime.Today.AddDays(-5);
            header = new DayTransactionsHeader();
            header.Add(1000);
            transactionList.Add(transactionDate, header);
            var testAccount = new SavingsAccount(transactionList);
            AssertDiff(0.0410966673012234129575946192m, testAccount.InterestAccrued());
        }
        [Test]
        public void StatementGenerationTest()
        {
            BaseAccount savingsAccount = new SavingsAccount();
            savingsAccount.Deposit(4000);
            savingsAccount.Withdraw(200);
            Assert.AreEqual(
                    "Savings Account\r\n" +
                    "  deposit $4,000.00\r\n" +
                    "  withdrawal $200.00\r\n" +
                    "Interests $0.02\r\n" +
                    "Total $3,800.02\r\n"
                    , savingsAccount.GetStatement());
        }
        [Test]
        public void StatementGenerationEmptyTest()
        {
            BaseAccount savingsAccount = new SavingsAccount();
            Assert.AreEqual(
                     "Savings Account\r\n" +
                    "Interests $0.00\r\n" +
                    "Total $0.00\r\n"
                    , savingsAccount.GetStatement());
        }
    }
}
