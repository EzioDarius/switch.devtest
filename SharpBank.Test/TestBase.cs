﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SharpBank.Test
{
    public class TestBase
    {
        private decimal DECIMAL_DELTA => 1e-15m;
        protected void AssertDiff(decimal a, decimal b)
        {
            Assert.IsTrue(Math.Abs(a - b) < DECIMAL_DELTA);
        }
    }
}
