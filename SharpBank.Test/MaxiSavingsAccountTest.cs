﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class MaxiSavingsAccountTest: TestBase
    {
        [Test]
        public void InterestAccruedInDaysTest()
        {
            var transactionList = new Dictionary<DateTime, DayTransactionsHeader>();
            var transactionDate = DateTime.Today.AddDays(-11);
            var header = new DayTransactionsHeader();
            header.Add(100);
            header.Add(-50);
            transactionList.Add(transactionDate, header);
            transactionDate = DateTime.Today.AddDays(-5);
            header = new DayTransactionsHeader();
            header.Add(400);
            transactionList.Add(transactionDate, header);
            var testAccount = new MaxiSavingsAccount(transactionList);
            AssertDiff(0.0686311446872450659744790937m, testAccount.InterestAccrued());
        }
    }
}
