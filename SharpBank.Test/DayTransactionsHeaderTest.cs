﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class DayTransactionsHeaderTest
    {
        public DayTransactionsHeader header { get; private set; }
        [SetUp]
        public void Init()
        {
            header = new DayTransactionsHeader();
        }
        [Test]
        public void AddAmountTest()
        {
            header.Add(200);
            Assert.AreEqual(200m, header.Movements.First());
        }
        [Test]
        public void SumAmountTest()
        {
            header.Add(200);
            header.Add(100);
            Assert.AreEqual(300m, header.AmountSum);
        }
        [Test]
        public void WithDrawFlagTest()
        {
            header.Add(200);
            header.Add(-100);
            Assert.IsTrue(header.HasWithdraw);
        }
        [TearDown]
        public void TearDown()
        {
            header = null;
        }
    }
}
