﻿using System.Collections.Generic;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest: TestBase
    {
        public Customer oscar { get; private set; }
        [SetUp]
        public void Init()
        {
            oscar = new Customer("Oscar");
        }
        [Test]
        public void TestCustomerStatementGeneration()
        {
            BaseAccount checkingAccount = new CheckingAccount();
            BaseAccount savingsAccount = new SavingsAccount();
            oscar.OpenAccount(checkingAccount).OpenAccount(savingsAccount);
            checkingAccount.Deposit(100);
            savingsAccount.Deposit(4000);
            savingsAccount.Withdraw(200);
            Assert.AreEqual("Statement for Oscar\r\n" +
                    "\r\n" +
                    "Checking Account\r\n" +
                    "  deposit $100.00\r\n" +
                    "Interests $0.00\r\n" +
                    "Total $100.00\r\n" +
                    "\r\n" +
                    "Savings Account\r\n" +
                    "  deposit $4,000.00\r\n" +
                    "  withdrawal $200.00\r\n" +
                    "Interests $0.02\r\n" +
                    "Total $3,800.02\r\n" +
                    "\r\n" +
                    "Total In All Accounts $3,900.02", oscar.GetStatement());
        }
        [Test]
        public void TestEmptyStatement()
        {
            Assert.AreEqual("Statement for Oscar\r\n\r\n" +
                    "Total In All Accounts $0.00", oscar.GetStatement());
        }
        [Test]
        public void TotalInterestEarnedTest()
        {
            oscar.OpenAccount(new CheckingAccount().Deposit(100));
            AssertDiff(0.00027397260273972602739726m, oscar.TotalInterestEarned());
        }

        [Test]
        public void TotalInterestEarnedEmptyTest()
        {
            Assert.AreEqual(0, oscar.TotalInterestEarned());
        }
        [Test]
        public void TestManyAccountsAdditions()
        {
            oscar.OpenAccount(new SavingsAccount());
            oscar.OpenAccount(new CheckingAccount());
            oscar.OpenAccount(new MaxiSavingsAccount());
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [TearDown]
        public void TearDown()
        {
            oscar = null;
        }

    }
}
